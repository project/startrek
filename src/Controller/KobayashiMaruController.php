<?php

namespace Drupal\startrek\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class KobayashiMaruController.
 *
 * @package Drupal\startrek\Controller
 */
class KobayashiMaruController extends ControllerBase {

  /**
   * Test.
   *
   * @return string
   *   Return Shacha string.
   */
  public function test() {
    \Drupal::service('startrek.enterprise.message')->setLoginMessage();
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement https://www.lucidchart.com/pages/flowcharts/star-trek'),
    ];
  }

}
